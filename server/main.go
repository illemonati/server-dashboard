package main

import (
	"server-dashboard-backend/router"
)


func main() {
	r := router.CreateRouter()
	_ = r.Run(":2313")
}
