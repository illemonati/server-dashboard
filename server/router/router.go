package router

import "github.com/gin-gonic/gin"
import "server-dashboard-backend/providers"
import "github.com/gin-contrib/static"

func CreateRouter()  *gin.Engine {
	r := gin.Default()

	r.Use(static.Serve("/", static.LocalFile("./client/build/", false)))


	r.GET("/api/sysinfo", func(c *gin.Context) {
		if info, err := providers.GetSystemInfo(); err != nil {
			c.JSON(500, gin.H{
				"error": err.Error(),
			})
		} else {
			c.JSON(200, info)
		}
	})
	return r
}