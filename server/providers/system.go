package providers

import (
	"github.com/elastic/go-sysinfo"
	"github.com/elastic/go-sysinfo/types"
)

func GetSystemInfo() (types.HostInfo, error){
	host, err := sysinfo.Host()
	if err != nil {
		return types.HostInfo{}, err
	}
	info := host.Info()
	return info, nil
}