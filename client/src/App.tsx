import React from 'react';
import './App.css';
import MainInfoPage from "./components/MainInfoPage";
import NavBar from "./components/NavBar";

const App: React.FC = () => {
  return (
    <div className="App">
        <MainInfoPage/>
    </div>
  );
};

export default App;
