import React, {useEffect, useState} from "react";
import NavBar from "../NavBar";
import {Card} from "@material-ui/core";
import './styles.css';
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";

interface OsInfo {
    family: string,
    platform: string,
    name: string,
    version: string,
    major: string,
    minor: string,
    patch: string,
    build?: string,
    codename?: string
}


interface SystemInfo {
    architecture: string,
    boot_time: string,
    containerized?: boolean,
    name: string,
    ip?: Array<String>,
    kernel_version: string,
    mac: string,
    os: OsInfo,
    timezone: string,
    timezone_offset_sec: number,
    id?: string
}


const MainInfoPage = () => {
    const [systemInfo, setSystemInfo] = useState({} as SystemInfo);

    const getSysInfo = async () => {
        const response = await fetch("http://localhost:2313/api/sysinfo");
        return await response.json();
    };

    useEffect(() => {
        getSysInfo().then((json) => {
            setSystemInfo(json as SystemInfo);
        })
    }, []);

    return (
        <div className="Main Information">
            <NavBar currentTitle="MainInfoPage"/>
            <h1>MainInfoPage</h1>
            <GridList  className="gridList" cols={3}>
            {Object.keys(systemInfo).map((category) => {
                if (category !== "os")
                return (
                    <GridListTile cols={1}>
                        <Card className="card" variant="outlined">
                            <CardContent>
                                <Typography className="title" variant="h5">
                                    {category}
                                </Typography>
                                <Typography variant="body2" component="p">
                                    {/*
                                    //@ts-ignore */}
                                    {systemInfo[category]}
                                </Typography>

                            </CardContent>
                        </Card>
                    </GridListTile>
                );
                else return Object.keys(systemInfo.os).map((osCat: string) => {
                        //@ts-ignore
                        const catItem = systemInfo.os[osCat];
                        return (
                            <GridListTile>
                                <Card className="card" variant="outlined">
                                    <CardContent>
                                        <Typography className="title" variant="h5">
                                            {osCat}
                                        </Typography>
                                        <Typography variant="body2" component="p">
                                            {catItem}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </GridListTile>
                        )
                    })

            })}
            </GridList>
        </div>
    )
};

export default MainInfoPage;

