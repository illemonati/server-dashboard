#! /usr/bin/env python3

import subprocess
import sys
import os


def build():
    print(os.path.join(os.path.abspath(sys.path[0]), "client/"))
    subprocess.Popen("yarn", cwd=os.path.join(os.path.abspath(sys.path[0]), "client/"), shell=True)
    subprocess.Popen("yarn build", cwd=os.path.join(os.path.abspath(sys.path[0]), "client/"), shell=True)
    subprocess.Popen(r'go build -ldflags="-s -w" -o server .', cwd=os.path.join(os.path.abspath(sys.path[0]), "server/"), shell=True)


def run():
    subprocess.call("./server/server")


def main():
    args = sys.argv
    if len(args) < 2:
        print("please use a command: [build, run]")
        return
    if args[1] == "build":
        build()
        return
    if args[1] == "run":
        build()
        run()


if __name__ == '__main__':
    main()

